package sesion5;

public interface Bonito { // no tiene atributos, tiene constantes y metodos abstractos
    static final String facultad="FIIS"; // static final lo convierten en una constante
    public abstract Byte[] fotografiar(String a); //no es obligatorio poner abstract en interface
    default void viajar(){
        System.out.println("ESTOY VOLANDO");
    }

}
